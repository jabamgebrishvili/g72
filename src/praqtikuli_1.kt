data class Point(val x: Int, val y: Int)

fun main(args: Array<String>) {

    val abscisa_da_ordinata1 = Point(8, 5)
    println(abscisa_da_ordinata1.toString())

    val abscisa_da_ordinata2 = Point(8, 5)
    println(abscisa_da_ordinata2.toString())

    if (abscisa_da_ordinata1.equals(abscisa_da_ordinata2) == true)
        println("პირველი ობიექტი უდრის მეორე ობიექტს")
    else
        println("პირველი ობიექტი არ უდრის მეორე ობიექტს")

    val gadacemuli_erteuli_x_ze = 5
    val gadacemuli_erteuli_y_ze = 2
    println("წერტილი გადაადგილდება აბსცისათა ღერძის მიმართ ${gadacemuli_erteuli_x_ze} ერთეულით და ორდინატთა ღერძის მიმართ ${gadacemuli_erteuli_y_ze} ერთეულით")


}

    //ბატონო ნიკოლოზ, წილადებზე ბევრი ვიწვალე, 5 დღე ვწერდი სხვადასხვა კოდს, თუმცა არცერთი არგაეშვა ან ერორებს აგდებდა
    //თქვენი მითითება და მონაწერი, რა თქმა უნდა, გავითვალისწინე, მაგრამ ალბათ ზუსტად ვერ მივხვდი რა უნდა გამეკეთებინა
    //მადლობა თქვენ ყურადღებისთვის!
/*
data class Fraction(val numerator: Int, val denominator: Int)

fun Fraction(args: Array<String>) {

    val numerator = 2
    val denominator = 6
    val shekvece = numerator / denominator
        println("შეკვეცილი წილადი უდრის $shekvece")


}
*/


















































